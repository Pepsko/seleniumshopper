package com.pepe.Shopper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class IpAdress {

    public static String getCity() throws IOException {
        String json = getJson();
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> decoded = gson.fromJson(json, type);

        return decoded.get("city");
    }

    private static String getJson() throws IOException {
        URL url = new URL("http://ip-api.com/json");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String inline;
        while((inline = br.readLine())!=null){
            sb.append(inline);
        }
        return sb.toString();
    }

}
